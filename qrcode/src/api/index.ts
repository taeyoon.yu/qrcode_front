import Axios from "axios";

function addAssetData(assetData): any {
  return Axios.post("/api/assetData/add", assetData);
}

function sendExcelData(assetInformations): any {
  return Axios.post("/api/excelAssetData/regist", assetInformations);
}

function getTotalAssetData(): any {
  return Axios.get("/api/totalAssetData/get");
}

function updateAssetData(assetData): any {
  return Axios.post("/api/assetData/update", assetData);
}

function sendJsonData(assetInformations): any {
  return Axios.post("/api/jsonAssetData/regist", assetInformations);
}

export {
  addAssetData,
  sendExcelData,
  getTotalAssetData,
  updateAssetData,
  sendJsonData
};
