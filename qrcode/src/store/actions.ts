import { getTotalAssetData, sendExcelData, sendJsonData } from "../api/index";

export default {
  ASSET_INFORMATION_FROM_FILE({ commit }, args: any) {
    commit("SET_ASSET_INFORMATION", args);
  },
  GET_TOTAL_ASSET_DATA(context) {
    getTotalAssetData().then(({ data }) => {
      context.commit("SET_ASSET_INFORMATION", data);
    });
  },
  SEND_EXCEL_DATA(context, data) {
    sendExcelData(data).then(({ data }) => {
      context.commit("SET_ASSET_INFORMATION", data);
    });
  },
  SEND_JSON_DATA(context, data) {
    sendJsonData(data).then(({ data }) => {
      context.commit("SET_ASSET_INFORMATION", data);
    });
  }
};
